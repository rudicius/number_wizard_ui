﻿using UnityEngine;
using System.Collections;

public class NumberWirzards : MonoBehaviour {

	int maxStart = 1000;
	int minStart = 1;
	int guess, max, min;

	// Use this for initialization
	void Start () {
		StartGame();					
	}

	void StartGame(){
		max = maxStart + 1;
		min = minStart;
		guess = (maxStart+minStart)/2;
		print("=========================");
		print ("Welcome to Number Wizard");
		print ("Pick a number in your head but don't tell me!");

		print ("The highest number you can pick is " + maxStart);
		print ("The lowest number you can pick is " + minStart);

		CalculateGuess();
	}
	 
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown(KeyCode.UpArrow)) {
			min = guess;
			CalculateGuess();
		} else if (Input.GetKeyDown(KeyCode.DownArrow)) {			
			max = guess;
			CalculateGuess();
		} else if (Input.GetKeyDown(KeyCode.Return)) {
			print ("I won");
			StartGame();
		}
					
	
	}

	void CalculateGuess(){
		guess = (min + max) / 2;
		print ("Is the number higher or lower than " + guess + "?");
		print ("Up arrow for higher, down for lower, return for equal");
	}
}
