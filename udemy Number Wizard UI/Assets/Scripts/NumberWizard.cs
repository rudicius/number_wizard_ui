﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class NumberWizard : MonoBehaviour {

	int maxStart = 1000;
	int minStart = 1;
	int guess, max, min;

    public int maxGuessesAllowed = 5;
    public float halfChance = 0.5f;
    public Text guessLabel;

	// Use this for initialization
	void Start () {
		StartGame();					
	}

	void StartGame(){
		max = 1000;
		min = 1;
        guess = Random.Range(min, max);
        guessLabel.text = guess.ToString();

        max = max + 1;
	}
	 
	
    public void GuessHigher()
    {
        min = guess;
        CalculateGuess();
    }

    public void GuessLower()
    {
        max = guess;
        CalculateGuess();
    }

    public void CorrectGuess()
    {
        SceneManager.LoadScene("LoseScene");
    }

    void CalculateGuess(){
		// guess = (min + max) / 2;
        
        maxGuessesAllowed = maxGuessesAllowed - 1;
        if (maxGuessesAllowed <= 0)
        {
            SceneManager.LoadScene("WinScene");
        }

        float randomNumber = Random.Range(0.0f, 1.0f);
        if ( randomNumber < halfChance)
        {
            guess = (min + max) / 2;
            Debug.Log("Using 1/2 guess: " + randomNumber);
        } else
        {
            guess = Random.Range(min, max);
            Debug.Log("Using random guess: " + randomNumber);
        }        
        guessLabel.text = guess.ToString();
        
	}
}
