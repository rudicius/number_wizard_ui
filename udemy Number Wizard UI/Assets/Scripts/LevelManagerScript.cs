﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelManagerScript : MonoBehaviour {

	public void LoadLevel (string name)
    {
        Debug.Log("Level load request for: " + name);
        SceneManager.LoadScene(name);        
    }

    public void QuitRequest ()
    {
        Debug.Log("Quit requested");
        Application.Quit();
    }

}
